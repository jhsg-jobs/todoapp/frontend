import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Todo } from '../models/todo.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TodoService {

  private apiUrl = 'https://jhsg-todo.herokuapp.com/api/todos';

  constructor(private http: HttpClient) { }

  public getAllTodo(filter): Observable<Todo[]> {
    return this.http.get<Todo[]>(this.apiUrl, {
      params: {
        filter: filter != null ? filter : ''
      }
    });
  }

  public createTodo(todo: Todo): Observable<any> {
    return this.http.post(this.apiUrl, todo);
  }

  public updateTodo(todo: Todo): Observable<any> {
    return this.http.put(`${this.apiUrl}/${todo.id}`, todo);
  }

  public deleteTodo(id: number): Observable<any> {
    return this.http.delete(`${this.apiUrl}/${id}`);
  }

  public deleteTodosDone(): Observable<any> {
    return this.http.delete(`${this.apiUrl}/done`);
  }
}
