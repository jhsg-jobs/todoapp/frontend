import { Observable } from 'rxjs';
import { TodoService } from './services/todo.service';
import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Todo } from './models/todo.model';
import { faTrashAlt } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  faTrashAlt = faTrashAlt;

  public todos: Observable<Todo[]>;
  public title: String = 'Todo List';
  public todoForm: FormGroup;

  /**
   *
   */
  constructor(private formBuilder: FormBuilder, private todoService: TodoService) {
    this.todoForm = this.formBuilder.group({
      title: ['', Validators.compose([
        Validators.minLength(3),
        Validators.maxLength(60),
        Validators.required,
      ])]
    });

    this.load(null);
  }

  load(filter) {
    this.todos = this.todoService.getAllTodo(filter);
    console.log(this.todos);
  }

  create() {
    const title = this.todoForm.controls.title.value;

    const request = this.todoService.createTodo(new Todo(null, title, false));
    request.subscribe(
      (data: Todo) => {
        this.load(null);
        this.clear();
      },
      (error) => {
        console.log(error);
      }
    );
  }

  clear() {
    this.todoForm.reset();
  }

  update(todo: Todo, checkbox) {
    todo.done = checkbox.target.checked;
    const request = this.todoService.updateTodo(todo);

    request.subscribe(
      (data: Todo) => {
        this.load(null);
      },
      (error) => {
        console.log(error);
      }
    );
  }

  remove(todo: Todo) {
    const request = this.todoService.deleteTodo(todo.id);

    request.subscribe(
      (data: Todo) => {
        this.load(null);
      },
      (error) => {
        console.log(error);
      }
    );
  }

  removeDone() {
    const request = this.todoService.deleteTodosDone();
    request.subscribe(
      (data) => {
        this.load(null);
      },
      (error) => {
        console.log(error);
      }
    );
  }

}
